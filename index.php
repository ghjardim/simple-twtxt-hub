<?php
date_default_timezone_set('UTC');

// Set the configuration variables in an associative array
define('CONFIG', [
	'filterMode' => 'lastN',  // Set to 'date' to filter by the last month, or 'lastN' to show the last N posts
	'lastNPosts' => 20,  // The number of last posts to show when in 'lastN' mode
	'avatarCachePath' => '/tmp/simple-twtxt-hub/avatars/', // Set the cache directory path
	'ignoredFiles' => ['LICENSE.txt', 'README.txt'] // Files to ignore
]);

function proxyImageDownload($imageUrl) {
	$ch = curl_init();
	curl_setopt_array($ch, [CURLOPT_URL => $imageUrl, CURLOPT_RETURNTRANSFER => true, CURLOPT_FOLLOWLOCATION => true]);
	$imageData = curl_exec($ch);
	curl_close($ch);
	if ($imageData) {
		$contentType = get_headers($imageUrl, 1)['Content-Type'];
		$base64Image = base64_encode($imageData);
		$dataUri = "data:$contentType;base64,$base64Image";
		return $dataUri;
	}
	return false;
}

function getAvatarDataFromCache($url) {
	$hash = hash('sha256', $url);
	$cacheFile = CONFIG['avatarCachePath'] . $hash . '.cache';

	if (file_exists($cacheFile)) {
		return file_get_contents($cacheFile);
	}

	return false;
}

function storeAvatarDataInCache($url, $dataUri) {
	$hash = hash('sha256', $url);
	$cacheFile = CONFIG['avatarCachePath'] . $hash . '.cache';

	// Store the data URI in the cache file
	file_put_contents($cacheFile, $dataUri);
}

function getAvatarFromBlog($filePath) {
	$content = file_get_contents($filePath);
	$matches = [];
	if (preg_match('/# *avatar\s*=\s*(\S+)/i', $content, $matches)) {
		$imageUrl = $matches[1];

		// Check if the avatar is cached
		$cachedDataUri = getAvatarDataFromCache($imageUrl);
		if ($cachedDataUri) {
			return $cachedDataUri;
		}

		// Download the avatar and cache it
		$dataUri = proxyImageDownload($imageUrl);
		if ($dataUri) {
			storeAvatarDataInCache($imageUrl, $dataUri);
			return $dataUri;
		}
	}
	return null;
}

function renderTweet($message) {
	// Line breaks
	$message = str_replace(' ', '</br>', $message);

	// Citations
	$message = preg_replace('/@<(\S+)\s+(\S+)>/', '<a href="$2">@$1</a>', $message);

	// URLs
	$message = preg_replace(
		'/(?<!<a href="|<\/a>)(\bhttps?:\/\/\S+\b(\/?))/',
		'<a href="$1" target="_blank">$1</a>',
		$message);
	// The negative lookbehind (?<!<a href="|<\/a>) ensures that the URL
	// is not already within an anchor tag. This prevents interference
	// with citations that have links.

	return $message;
}

function getUsernameFromBlog($filePath) {
	$content = file_get_contents($filePath);
	$matches = [];
	if (preg_match('/# nick\s*=\s*(\S+)/i', $content, $matches)) {
		return $matches[1];
	}
	return null;
}

function extractPostInfo($line) {
	$matches = [];
	if (preg_match('/^(\S+)\s*(\(#(\w{7})\))?\s*(.*?)\s*$/', $line, $matches)) {
		$date = DateTime::createFromFormat('Y-m-d\TH:i:sP', $matches[1]);
		if (!$date) {
			// Attempt to parse with other formats if the default format fails
			$date = date_create($matches[1]);
		}

		$hash = null;
		if (isset($matches[3]) && ($matches[3] != "")) {
			$hash = '#' . $matches[3];
		}

		return [
			'date' => $date ? $date->format('Y-m-d\TH:i:sP') : null,
			'hash' => $hash,
			'content' => htmlspecialchars(renderTweet($matches[4]))
		];
	}
	return null;
}

function getPosts($directory) {
	$posts = [];

	if (is_dir($directory)) {
		if ($handle = opendir($directory)) {
			while (($file = readdir($handle)) !== false) {
				// Check if the file should be ignored
				if (in_array($file, CONFIG['ignoredFiles']) || pathinfo($file, PATHINFO_EXTENSION) !== 'txt') {
					continue;
				}

				$username = getUsernameFromBlog($file);
				$avatar = getAvatarFromBlog($file);
				$content = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

				foreach ($content as $line) {
					if (substr(trim($line), 0, 1) !== '#') {
						$postInfo = extractPostInfo($line);
						if ($postInfo !== null) {
							$postInfo['username'] = $username;
							$postInfo['avatar'] = $avatar;
							$postInfo['filename'] = $file;
							$posts[] = $postInfo;
						}
					}
				}
			}

			closedir($handle);
		} else {
			echo "Unable to open directory.";
		}
	} else {
		echo "Directory does not exist.";
	}

	// Sort posts by date in descending order
	usort($posts, function ($a, $b) {
		$dateA = new DateTimeImmutable($a['date']);
		$dateB = new DateTimeImmutable($b['date']);

		return $dateB <=> $dateA;
	});

	return $posts;
}

function listUsers($directory) {
	$users = [];

	$files = scandir($directory);
	foreach ($files as $file) {
		// Check if the file should be ignored
		if (in_array($file, CONFIG['ignoredFiles']) || pathinfo($file, PATHINFO_EXTENSION) !== 'txt') {
			continue;
		}

		$username = getUsernameFromBlog($file);
		if (!empty($username)) {
			$users[$username] = $file;
		}
	}

	return $users;
}

function filterPostsByDate($posts, $startDate, $endDate) {
	$filteredPosts = [];

	foreach ($posts as $post) {
		$postDate = new DateTimeImmutable($post['date']);

		// Set the time zone for filtering
		$timeZone = new DateTimeZone('UTC');
		$postDateFormatted = $postDate->setTimezone($timeZone)->format('Y-m-d\TH:i:sP');

		if ($postDate >= $startDate && $postDate <= $endDate) {
			$filteredPosts[] = $post;
		}
	}

	return $filteredPosts;
}

function getLastNPosts($posts, $n) {
	return array_slice($posts, 0, $n);
}

// Ensure the cache directory exists
if (!is_dir(CONFIG['avatarCachePath'])) {
	mkdir(CONFIG['avatarCachePath'], 0755, true);
}

$directory = '.';
$users = listUsers($directory);
$allPosts = getPosts($directory);

// Store the total number of posts before filtering
$totalPosts = count($allPosts);

if (CONFIG['filterMode'] === 'date') {
	// Define the start and end dates for the past month
	$endDate = new DateTime();
	$startDate = (new DateTime())->modify('-30 days');

	// Filter posts to include only those from the past month
	$filteredPosts = filterPostsByDate($allPosts, $startDate, $endDate);
} elseif (CONFIG['filterMode'] === 'lastN') {
	// Get the last N posts
	$filteredPosts = getLastNPosts($allPosts, CONFIG['lastNPosts']);
} else {
	echo "Invalid filter mode.";
	exit;
}

// Calculate the number of hidden posts
$hiddenPosts = $totalPosts - count($filteredPosts);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Twtxt Hub</title>
    <link rel="icon" href="/static/favicon.png" type="image/png">
	<style>
	body {
		font-family: monospace;
		margin-left: 10%;
		margin-right: 10%;
	}

	a { color: #004777; }

	.post-container {
		border: 1px solid #ccc;
		padding: 5px;
		margin-bottom: 5px;
	}

	.post-header {
		font-weight: bold;
		display: flex;
		align-items: center;
		gap: 5px;
	}

	.timestamp {
		color: #888;
		margin-right: 10px;
	}

	.hash,
	.cited-user {
		color: #888;
		font-style: italic;
	}

	.content {
		line-height: 1.4;
		margin: 10px 0 5px 0;
	}
</style>

</head>
<body>
	<h1>Twtxt Hub</h1>

	<hr>

	<h2>Users</h2>
<?php
if (empty($users)) {
	echo "No users yet<br>";
} else {
	foreach ($users as $username => $userFile) {
		$userLink = '<a href="' . $userFile . '">' . $username . '</a>';
		echo '<strong>' . $userLink . '</strong><br>';
	}
}
?>

	<hr>

	<h2>Timeline <?php echo CONFIG['filterMode'] === 'date' ? '(past month)' : "(last " . CONFIG['lastNPosts'] . " posts)"; ?></h2>
<?php
if (empty($filteredPosts)) {
	echo CONFIG['filterMode'] === 'date' ? "No posts in the past month<br>" : "No posts available<br>";
} else {
	foreach ($filteredPosts as $post) {
		$userLink = '<a href="' . $post['filename'] . '">' . $post['username'] . '</a>';
		if (is_null($post['avatar'])) {
			$avatarLink = null;
		} else {
			$avatarLink = '<img src="' . $post['avatar'] . '" height="24px" alt=""> ';
		}

		$date = $post['date'];
		$hash = $post['hash'];
		$content = html_entity_decode($post['content']);

		echo '<div class="post-container">';
		echo '<div class="post-header">';
			echo  $avatarLink;
			echo '<strong>' . $userLink . '</strong> ';
			echo '<span class="timestamp">' . $date . '</span> ';
		echo '</div>';

		echo '<div class="post-content">';
		if (!empty($hash)) {
			echo '<div class="hash">Thread: ' . $hash . '</div>';
		}
		echo '<p class="content">' . $content . '</p>';
		echo '</div>';

		echo '</div>';
	}
	// Display the hidden posts message
	echo "<p><small>";
	if (CONFIG['filterMode'] === 'date') {
		echo "Only posts in the last month are displayed.";
	} elseif (CONFIG['filterMode'] === 'lastN') {
		echo "Only the last " . CONFIG['lastNPosts'] . " posts are displayed.";
	}
	echo " <strong>$hiddenPosts are hidden.</strong> Consider following specific user to see all of his posts.";
	echo "</small></p>";
}
?>

   <hr>
   <small>This project is a web platform hosting twtxt blogs for users, providing a timeline aggregator to display their posts and profiles in chronological order.</small>
</body>
</html>
