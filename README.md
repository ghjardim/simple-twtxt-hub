# Simple TWTXT hub

This software aims to be a simple and minimalist solution for rendering
hosted twtxt feeds. It was created as a _somewhat_ alternative to Yarn
and Yarnd, as I found them both as too complicated and potentially
resource-intensive.

As this software is now functional, it is still in early development.
